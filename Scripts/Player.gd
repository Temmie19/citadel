extends KinematicBody

const GRAVITY = -70
var vel = Vector3()
const MAXSPEED = 35
const JUMPSPEED = 25
const ACCEL = 10

var dir = Vector3()

const DEACCEL = 20
const MAXSLOPEANGLE = 40

var camera
var rotationHelper
var flashlight

var MOUSESENSITIVITY = 0.25


var animationManager

# Called when the node enters the scene tree for the first time.
func _ready():
	camera = $RotationHelper/Head/Camera
	rotationHelper = $RotationHelper
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	pass # Replace with function body.


func _process(delta):
	processInput(delta)
	processMovement(delta)
	pass

func processInput(delta):
	dir = Vector3()
	
	#Walking
	#______________________________________________
	var camXform = camera.get_global_transform()
	
	var inputMovementVector = Vector2()
	
	if Input.is_action_pressed("moveForward"):
		inputMovementVector.y += 1
		pass
	if Input.is_action_pressed("moveBackward"):
		inputMovementVector.y -= 1
		pass
	if Input.is_action_pressed("moveLeft"):
		inputMovementVector.x -= 1
		pass
	if Input.is_action_pressed("moveRight"):
		inputMovementVector.x += 1
		pass
	inputMovementVector = inputMovementVector.normalized()
	
	dir += -camXform.basis.z * inputMovementVector.y
	dir += camXform.basis.x * inputMovementVector.x
	
	#Jumping
	#_______________________________________
	if is_on_floor():
		if Input.is_action_just_pressed("jump"):
			vel.y = JUMPSPEED
			pass
		pass
	
	#Capturing and freeing the cursor
	#__________________________________________________
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			pass
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			pass
		pass
	pass



func processMovement(delta):
	dir.y = 0
	dir = dir.normalized()
	
	vel.y += delta * GRAVITY
	
	var hvel = vel
	var avel = 1.05
	
	hvel.y = 0
	
	var target = dir
	
	target *= MAXSPEED
	
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
		pass
	else:
		accel = DEACCEL
		pass
	
	hvel = hvel.linear_interpolate(target, accel * delta)
	if is_on_floor():
		avel = 1.05
		vel.x = hvel.x
		vel.z = hvel.z
		pass
	elif not is_on_floor():
#		if Input.is_action_pressed("moveLeft"):
#			avel = avel * 1.01
#			vel.x = hvel.x * avel
#			vel.z = hvel.z * avel
#		elif Input.is_action_pressed("moveRight"):
#			avel = avel * 1.01
#			vel.x = hvel.x * avel
#			vel.z = hvel.z * avel
#		else:
		vel.x = hvel.x * 1.05
		vel.z = hvel.z * 1.05
		pass
	vel = move_and_slide(vel, Vector3(0,1,0), 0.05, 4, deg2rad(MAXSLOPEANGLE))
	pass


func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotationHelper.rotate_x(deg2rad(event.relative.y * MOUSESENSITIVITY))
		self.rotate_y(deg2rad(event.relative.x * MOUSESENSITIVITY * -1))
		pass
		
		var cameraRot = rotationHelper.rotation_degrees
		cameraRot.x = clamp(cameraRot.x, -70, 70)
		rotationHelper.rotation_degrees = cameraRot
		pass
	pass
